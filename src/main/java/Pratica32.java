
/**
 *
 * @author Leandro Augusto de Carvalho
 */
public class Pratica32 {
    public static double densidade(double x, double media, double desvio) {
        double d = (1/(Math.sqrt(2*Math.PI)*desvio)*Math.exp(-1*(Math.pow(x-media,2)/Math.pow(2*desvio,2))));
        return d;
    }

    public static void main(String[] args) {
        System.out.println(densidade(-1,67,3));        
    }
}
